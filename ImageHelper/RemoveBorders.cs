﻿using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageHelper
{
    public class RemoveBorders
    {
        public static string Start(string file, int countFrames, int treshold, string outputFolder)
        {
            var bitmap = new Bitmap(file);
            var widthOfFrame = bitmap.Width / countFrames;

            //detect common borders
            //left 
            var left = 0;
            for (var x = 0; x < widthOfFrame && left == 0; x++)
            {
                for (var y = 0; y < bitmap.Height && left == 0; y++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        if((pixel.R > treshold || pixel.G > treshold || pixel.B > treshold) && pixel.A > treshold)
                        {
                            left = x;
                            break;
                        }
                    }
                }
            }

            //right
            var right = 0;
            for (var x = widthOfFrame - 1; x >= 0 && right == 0; x--)
            {
                for (var y = 0; y < bitmap.Height && right == 0; y++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        if ((pixel.R > treshold || pixel.G > treshold || pixel.B > treshold) && pixel.A > treshold)
                        {
                            right = x;
                            break;
                        }
                    }
                }
            }
            
            //top 
            var top = 0;
            for (var y = 0; y < bitmap.Height && top == 0; y++)
            {
                for (var x = 0; x < widthOfFrame && top == 0; x++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        if ((pixel.R > treshold || pixel.G > treshold || pixel.B > treshold) && pixel.A > treshold)
                        {
                            top = y;
                            break;
                        }
                    }
                }
            }

            //bottom 
            var bottom = 0;
            for (var y = bitmap.Height - 1; y >= 0 && bottom == 0; y--)
            {
                for (var x = 0; x < widthOfFrame && bottom == 0; x++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        if ((pixel.R > treshold || pixel.G > treshold || pixel.B > treshold) && pixel.A > treshold)
                        {
                            bottom = y;
                            break;
                        }
                    }
                }
            }

            //cut all frames 
            var newWidthOfFrame = right - left + 1;
            var newBitmap = new Bitmap(newWidthOfFrame * countFrames, bottom - top + 1);
            for (var x = left; x <= right; x++)
            {
                for (var y = top; y <= bottom; y++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        newBitmap.SetPixel((x - left) + newWidthOfFrame * frame, y - top, pixel);
                    }
                }
            }

            var resultPath = Path.Combine(outputFolder, file.Split('\\').Last());
            newBitmap.Save(resultPath);

            return resultPath;
        }
    }
}
