﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageHelper
{
    public class JoinLogic
    {
        public static string Start(List<string> files, string outputFolder)
        {
            if(files?.Any() != true)
            {
                return null;
            }

            var images = new List<Image>();
            foreach (var file in files)
            {
                images.Add(Image.FromFile(file));
            }

            var newWidth = images.Sum(x => x.Width);
            var newHeight = images.Max(x => x.Height);

            Bitmap bitmap = new Bitmap(newWidth, newHeight);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                var x = 0;
                foreach (var image in images)
                {
                    g.DrawImage(image, x, 0);
                    x += image.Width;
                }
            }

            var resultPath = Path.Combine(outputFolder, files[0].Split('\\').Last());
            bitmap.Save(resultPath);
            return resultPath;
        }
    }
}
