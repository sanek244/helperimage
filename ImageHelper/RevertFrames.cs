﻿using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageHelper
{
    public class RevertFrames
    {
        public static string Start(string file, int countFrames, string outputFolder)
        {
            var bitmap = new Bitmap(file);
            var widthOfFrame = bitmap.Width / countFrames;
            var newBitmap = new Bitmap(bitmap.Size.Width, bitmap.Size.Height);
            for (var x = 0; x < widthOfFrame; x++)
            {
                for (var y = 0; y < bitmap.Size.Height; y++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        newBitmap.SetPixel(x + widthOfFrame * (countFrames - frame - 1), y, pixel);
                    }
                }
            }

            var resultPath = Path.Combine(outputFolder, file.Split('\\').Last());
            newBitmap.Save(resultPath);

            return resultPath;
        }
    }
}
