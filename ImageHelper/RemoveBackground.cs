﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageHelper
{
    public class RemoveBackground
    {
        public static bool Start(List<string> files, int threshold, string outputFolder, bool isAlphaTransform = false)
        {
            if(files?.Any() != true)
            {
                return false;
            }

            foreach (var file in files)
            {
                var bitmap = new Bitmap(file);

                for(var x = 0; x < bitmap.Width; x++)
                {
                    for (var y = 0; y < bitmap.Height; y++)
                    {
                        var pixel = bitmap.GetPixel(x, y);

                        if (isAlphaTransform)
                        {
                            var sum = pixel.R + pixel.G + pixel.B;
                            bitmap.SetPixel(x, y, Color.FromArgb(sum / 3, pixel.R, pixel.G, pixel.B));
                        }
                        else
                        {
                            if (pixel.R < 255 * threshold / 100 ||
                                pixel.G < 255 * threshold / 100 ||
                                pixel.B < 255 * threshold / 100)
                            {
                                bitmap.SetPixel(x, y, Color.FromArgb(0, 0, 0, 0));
                            }
                        }
                    }
                }

                var resultPath = Path.Combine(outputFolder, file.Split('\\').Last());
                bitmap.Save(resultPath);
            }

            return true;
        }
    }
}
