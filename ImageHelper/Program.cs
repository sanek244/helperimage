﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageHelper
{
    class Program
    {
        protected static readonly string InputFolder = "input";
        protected static readonly string OutputFolder = "output";

        static void Main(string[] args)
        {
            var projectDirectory = Environment.CurrentDirectory;
            var inputDirectory = Path.Combine(projectDirectory, InputFolder);
            var outputDirectory = Path.Combine(projectDirectory, OutputFolder);

            //create folders
            if (!Directory.Exists(inputDirectory))
                Directory.CreateDirectory(inputDirectory);

            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            Console.WriteLine($"input directory: '{inputDirectory}'");
            Console.WriteLine($"output directory: '{outputDirectory}'");

            var inputFiles = Directory.GetFiles(inputDirectory).OrderBy(x => x.Length).ThenBy(x => x).ToList();
            Console.WriteLine($"input files : {inputFiles?.Count()}");
            inputFiles.ToList().ForEach(x => Console.WriteLine("\t" + x.Split('\\').Last()));

            if(inputFiles?.Any() != true)
            {
                Console.WriteLine("No Files to process!");
                Console.ReadKey();
                return;
            }

            Process.Start(inputDirectory);

            Console.WriteLine();
            Console.WriteLine("1. Remove black background");
            Console.WriteLine("2. Transform black background to Alpha");
            Console.WriteLine("3. Cut borders");
            Console.WriteLine("4. Cut borders - manual");
            Console.WriteLine("5. Join");
            Console.WriteLine("6. Revert frames");
            Console.Write("Your select: ");

            var result = Console.ReadKey();
            Console.WriteLine("");
            try
            {
                switch (result.Key)
                {
                    case ConsoleKey.D1:
                        Console.WriteLine("Enter threshold % value (0-100):");
                        var threshold = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Start removing background...");
                        var result1 = RemoveBackground.Start(inputFiles, int.Parse(threshold), outputDirectory);
                        if (result1)
                        {
                            Console.WriteLine($"Done.");
                        }
                        else
                        {
                            Console.WriteLine($"No files");
                        }
                        break;

                    case ConsoleKey.D2:
                        Console.WriteLine("Start transform background...");
                        var result2 = RemoveBackground.Start(inputFiles, 0, outputDirectory, true);
                        if (result2)
                        {
                            Console.WriteLine($"Done.");
                        }
                        else
                        {
                            Console.WriteLine($"No files");
                        }
                        break;

                    case ConsoleKey.D3:
                        Console.WriteLine();
                        Console.WriteLine("Start cut borders...");
                        foreach(var file in inputFiles)
                        {
                            Console.WriteLine($"File: " + file);
                            Console.Write($"Enter count frames: ");
                            var countFramesStr = Console.ReadLine();
                            int.TryParse(countFramesStr, out int countFrames);
                            if(countFrames <= 0)
                            {
                                throw new Exception("Entered count frames is not valid");
                            }

                            var resultPath3 = RemoveBorders.Start(file, countFrames, 10, outputDirectory);
                            Console.WriteLine($"Done. Result is saved to '{resultPath3}'");
                            Console.WriteLine();
                        }
                        break;

                    case ConsoleKey.D4:
                        Console.WriteLine();

                        foreach (var file in inputFiles)
                        {
                            Console.WriteLine($"File: " + file);

                            Console.WriteLine("Enter width by center of new frame:");
                            var newWIdthStr = Console.ReadLine();
                            int.TryParse(newWIdthStr, out int newWIdth);

                            Console.WriteLine("Enter top:");
                            var topStr = Console.ReadLine();
                            int.TryParse(topStr, out int top);

                            Console.WriteLine("Enter bottom space size:");
                            var bottomStr = Console.ReadLine();
                            int.TryParse(bottomStr, out int bottom);

                            Console.Write($"Enter count frames: ");
                            var countFramesStr = Console.ReadLine();
                            int.TryParse(countFramesStr, out int countFrames);
                            if (countFrames <= 0)
                            {
                                throw new Exception("Entered count frames is not valid");
                            }

                            var resultPath3 = RemoveBordersManual.Start(file, countFrames, newWIdth, top, bottom, outputDirectory);
                            Console.WriteLine($"Done. Result is saved to '{resultPath3}'");
                            Console.WriteLine();
                        }
                        break;

                    case ConsoleKey.D5:
                        Console.WriteLine("Start join...");
                        var resultPath4 = JoinLogic.Start(inputFiles, outputDirectory);
                        if (!string.IsNullOrEmpty(resultPath4))
                        {
                            Console.WriteLine($"Done. Result is saved to '{resultPath4}'");
                        }
                        else
                        {
                            Console.WriteLine($"No files");
                        }
                        break;

                    case ConsoleKey.D6:
                        Console.WriteLine();
                        Console.WriteLine("Start revert frames...");
                        foreach (var file in inputFiles)
                        {
                            Console.WriteLine($"File: " + file);
                            Console.Write($"Enter count frames: ");
                            var countFramesStr = Console.ReadLine();
                            int.TryParse(countFramesStr, out int countFrames);
                            if (countFrames <= 0)
                            {
                                throw new Exception("Entered count frames is not valid");
                            }

                            var resultPath7 = RevertFrames.Start(file, countFrames, outputDirectory);
                            Console.WriteLine($"Done. Result is saved to '{resultPath7}'");
                            Console.WriteLine();
                        }
                        break;

                    case ConsoleKey.D7:
                        Console.WriteLine("Start 7...");
                        var fileF = inputFiles.First();
                        var bitmap = new Bitmap(fileF);
                        var newBitmap = new Bitmap(bitmap.Width / 2, bitmap.Height);
                        var countFrames_ = 90;
                        var widthOfFrame = bitmap.Width / countFrames_;
                        for (var x = 0; x < widthOfFrame; x++)
                        {
                            for (var y = 0; y < bitmap.Height; y++)
                            {
                                for (var frame = 0; frame < countFrames_ / 2; frame++)
                                {
                                    var pixel = bitmap.GetPixel(x + widthOfFrame * (frame * 2), y);
                                    newBitmap.SetPixel(x + widthOfFrame * frame, y, pixel);
                                }
                            }
                        }

                        var resultPath5 = Path.Combine(outputDirectory, fileF.Split('\\').Last());
                        newBitmap.Save(resultPath5);
                        if (!string.IsNullOrEmpty(resultPath5))
                        {
                            Console.WriteLine($"Done. Result is saved to '{resultPath5}'");
                        }
                        else
                        {
                            Console.WriteLine($"No files");
                        }
                        break;

                    default:
                        Console.WriteLine("Unknown point of menu!");
                        break;
                }
            }
            catch(Exception err)
            {
                Console.WriteLine();
                Console.WriteLine("Error: " + err.Message);
            }

            Console.WriteLine("End.");
            Console.ReadKey();
        }
    }
}
