﻿using System.Drawing;
using System.IO;
using System.Linq;

namespace ImageHelper
{
    public class RemoveBordersManual
    {
        public static string Start(string file, int countFrames, int width, int top, int bottomSpace, string outputFolder)
        {
            var bitmap = new Bitmap(file);
            var widthOfFrame = bitmap.Width / countFrames;

            var left = (widthOfFrame - width) / 2;
            var right = widthOfFrame - left;
            var bottom = bitmap.Height - bottomSpace;

            //cut all frames 
            var newWidthOfFrame = right - left + 1;
            var newBitmap = new Bitmap(newWidthOfFrame * countFrames, bottom - top + 1);
            for (var x = left; x <= right; x++)
            {
                for (var y = top; y <= bottom; y++)
                {
                    for (var frame = 0; frame < countFrames; frame++)
                    {
                        var pixel = bitmap.GetPixel(x + widthOfFrame * frame, y);
                        newBitmap.SetPixel((x - left) + newWidthOfFrame * frame, y - top, pixel);
                    }
                }
            }

            var resultPath = Path.Combine(outputFolder, file.Split('\\').Last());
            newBitmap.Save(resultPath);

            return resultPath;
        }
    }
}
